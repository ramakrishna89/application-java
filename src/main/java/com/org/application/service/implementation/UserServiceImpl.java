package com.org.application.service.implementation;


import com.org.application.constants.ApplicationConst;
import com.org.application.dto.NotificationEmailDto;
import com.org.application.dto.PaginationDto;
import com.org.application.dto.UserDto;
import com.org.application.exception.APPException;
import com.org.application.model.Password;
import com.org.application.model.User;
import com.org.application.model.VerificationToken;
import com.org.application.props.ApplicationProps;
import com.org.application.repository.PasswordRepo;
import com.org.application.repository.RoleAccessRepo;
import com.org.application.repository.UserRepo;
import com.org.application.repository.VerificationTokenRepo;
import com.org.application.service.component.MailerComponent;
import com.org.application.service.interfaces.UserService;
import com.org.application.service.mapper.UserMapper;
import com.org.application.utils.DIUtils;
import com.org.application.utils.StaticUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepo userRepo;
    private final RoleAccessRepo roleAccessRepo;
    private final PasswordRepo passwordRepo;
    private final VerificationTokenRepo verificationTokenRepo;
    private final DIUtils diUtils;
    private final PasswordEncoder passwordEncoder;
    private final ApplicationProps applicationProps;
    private final MailerComponent mailerComponent;

    @Override
    public UserDto create(UserDto dto) throws Exception {
        User user = userMapper.toModel(dto);
        userRepo.findByEmail(dto.getEmail())
                .ifPresent(existingUser -> {
                    throw new APPException("app.error.msg-35");
                });

        user.setRoleAccess(roleAccessRepo.findById(dto.getSelectedRoleAccessId())
                .orElseThrow(() -> new APPException("app.error.msg-30")));

        String tempPassword = diUtils.generatePassay();
        Password password = Password.builder()
                .password(passwordEncoder.encode(tempPassword))
                .build();
        passwordRepo.save(password);
        user.setPassword(password);
        userRepo.save(user);

        String token = StaticUtils.generateUUID();
        VerificationToken verificationToken = VerificationToken.builder()
                .token(token)
                .user(user)
                .build();
        verificationTokenRepo.save(verificationToken);

        NotificationEmailDto emailDto = NotificationEmailDto.builder()
                .type(ApplicationConst.app_mail_activation)
                .recipient(user.getEmail())
                .activationUrl((applicationProps.getDomainURL() + applicationProps.getActivationLink() + token))
                .tempPassword(tempPassword).build();
        mailerComponent.sendMail(emailDto);
        return userMapper.toDto(user);
    }

    @Override
    public UserDto update(UserDto dto) throws Exception {
        return userMapper.toDto(userRepo.findById(dto.getId())
                .map(model -> {
                    BeanUtils.copyProperties(dto, model);
                    model.setRoleAccess(roleAccessRepo.findById(dto.getSelectedRoleAccessId())
                            .orElseThrow(() -> new APPException("app.error.msg-30")));
                    userRepo.save(model);
                    return model;
                }).orElseThrow(() -> new APPException("app.error.msg-17")));
    }

    @Override
    public PaginationDto readAll(PaginationDto dto) throws Exception {
        Page<User> page;
        if (StringUtils.hasText(dto.getSearchField()) && StringUtils.hasText(dto.getSearchValue())) {
            page = userRepo.findAll(userRepo.searchSpec(dto.getSearchField(), dto.getSearchValue()),
                    StaticUtils.generatePageRequest.apply(dto));
        } else {
            page = userRepo.findAll(StaticUtils.generatePageRequest.apply(dto));
        }
        dto.setTotal(page.getTotalElements());
        dto.setLoading(false);
        dto.setResult(page.stream()
                .map(entity -> userMapper.toDto(entity))
                .collect(Collectors.toList()));
        return dto;
    }

    @Override
    public UserDto read(Long id) throws Exception {
        return userMapper.toDto(userRepo.findByIdAndStatus(id, ApplicationConst.active, User.class).orElseThrow(
                () -> new APPException("app.error.msg-17")));
    }

    @Override
    public Boolean delete(Long id) throws Exception {
        userRepo.deleteById(id);
        return true;
    }

    @Override
    public Boolean changeStatus(Long id, boolean status) {
        return userRepo.findById(id)
                .map(object -> {
                    object.setStatus(status);
                    userRepo.save(object);
                    return true;
                }).orElseThrow(() -> new APPException("app.error.msg-17"));

    }

    @Override
    public Map<Long, String> getIdAndNameMap() throws Exception {
        return userRepo.findAllByStatus(ApplicationConst.active, User.class).stream()
                .collect(Collectors.toMap(User::getId, User::getName));
    }

    @Override
    public Boolean resendActivationMail(long id) {
        return userRepo.findByIdAndStatus(id, ApplicationConst.active, User.class)
                .map(user -> {
                    String token = StaticUtils.generateUUID();
                    VerificationToken verificationToken = VerificationToken.builder()
                            .token(token)
                            .user(user)
                            .build();
                    verificationTokenRepo.save(verificationToken);

                    String tempPassword = diUtils.generatePassay();
                    Password password = Password.builder()
                            .password(passwordEncoder.encode(tempPassword))
                            .build();
                    passwordRepo.save(password);
                    user.setPassword(password);
                    userRepo.save(user);

                    NotificationEmailDto emailDto = NotificationEmailDto.builder()
                            .type(ApplicationConst.app_mail_activation)
                            .recipient(user.getEmail())
                            .activationUrl((applicationProps.getDomainURL() + applicationProps.getActivationLink() + token))
                            .tempPassword(tempPassword).build();
                    mailerComponent.sendMail(emailDto);
                    return true;
                }).orElseThrow(() -> new APPException("app.error.msg-17"));
    }
}
