package com.org.application.service.implementation;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.PaginationDto;
import com.org.application.dto.RoleDataDto;
import com.org.application.exception.APPException;
import com.org.application.model.RoleData;
import com.org.application.repository.RoleDataRepo;
import com.org.application.service.interfaces.RoleDataService;
import com.org.application.service.mapper.RoleDataMapper;
import com.org.application.utils.StaticUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class RoleDataServiceImpl implements RoleDataService {


    private RoleDataRepo roleDataRepo;
    private RoleDataMapper roleDataMapper;

    @Override
    public RoleDataDto create(RoleDataDto dto) throws Exception {
        return roleDataMapper.toDto(roleDataRepo.save(roleDataMapper.toModel(dto)));
    }

    @Override
    public RoleDataDto update(RoleDataDto dto) throws Exception {
        return roleDataMapper.toDto(roleDataRepo.findById(dto.getId())
                .map(model -> {
                    BeanUtils.copyProperties(dto, model);
                    roleDataRepo.save(model);
                    return model;
                }).get());
    }

    @Override
    public PaginationDto readAll(PaginationDto dto) throws Exception {
        Page<RoleData> page;
        if (StringUtils.hasText(dto.getSearchField()) && StringUtils.hasText(dto.getSearchValue())) {
            page = roleDataRepo.findAll(roleDataRepo.searchSpec(dto.getSearchField(), dto.getSearchValue()),
                    StaticUtils.generatePageRequest.apply(dto));
        } else {
            page = roleDataRepo.findAll(StaticUtils.generatePageRequest.apply(dto));
        }
        dto.setTotal(page.getTotalElements());
        dto.setLoading(false);
        dto.setResult(page.stream()
                .map(entity -> roleDataMapper.toDto(entity))
                .collect(Collectors.toList()));
        return dto;
    }

    @Override
    public RoleDataDto read(Long id) throws Exception {
        return
                roleDataMapper.toDto(roleDataRepo.findByIdAndStatus(id, ApplicationConst.active, RoleData.class).orElseThrow(
                        () -> new APPException("app.error.msg-17")));
    }

    @Override
    public Boolean delete(Long id) throws Exception {
        roleDataRepo.deleteById(id);
        return true;
    }

    @Override
    public Boolean changeStatus(Long id, boolean status) {
        return roleDataRepo.findById(id)
                .map(object -> {
                    object.setStatus(status);
                    roleDataRepo.save(object);
                    return true;
                }).orElseThrow(() -> new APPException("app.error.msg-17"));

    }

    @Override
    public Map<Long, String> getIdAndNameMap() throws Exception {
        return roleDataRepo.findAllByStatus(ApplicationConst.active, RoleData.class).stream()
                .collect(Collectors.toMap(RoleData::getId, RoleData::getName));
    }
}
