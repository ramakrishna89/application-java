package com.org.application.service.implementation;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.MenuDto;
import com.org.application.dto.NodeDto;
import com.org.application.dto.PaginationDto;
import com.org.application.exception.APPException;
import com.org.application.model.Menu;
import com.org.application.repository.MenuRepo;
import com.org.application.service.interfaces.MenuService;
import com.org.application.service.mapper.MenuMapper;
import com.org.application.utils.StaticUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class MenuServiceImpl implements MenuService {

    private final MenuRepo menuRepo;
    private final MenuMapper mapper;

    @Override
    public List<MenuDto> loadMenuForRole(Set<Long> roleIds) throws Exception {
        List<MenuDto> allMenuDtos = menuRepo.findDistinctByRoleAccesses_IdIsInOrderByIdAsc(roleIds)
                .stream()
                .map(menu -> mapper.mapToDto(menu))
                .collect(Collectors.toList());
        Map<Long, MenuDto> rootMenuDtos = allMenuDtos.stream()
                .filter(menuDto -> menuDto.getRootMenuId() == ApplicationConst.app_root_menu_id)
                .collect(Collectors.toMap(MenuDto::getId, Function.identity()));
        allMenuDtos.stream()
                .filter(allMenuDto -> allMenuDto.getRootMenuId() != ApplicationConst.app_root_menu_id)
                .forEach(allMenuDto -> rootMenuDtos.get(allMenuDto.getRootMenuId()).getSubMenuDtos().add(allMenuDto));
        return rootMenuDtos.values().stream().sorted().collect(Collectors.toList());
    }

    @Override
    public List<String> loadAllMenuTypes() throws Exception {
        return menuRepo.findDistinctMenuType();
    }

    @Override
    public List<NodeDto> loadMenusOfMenuType(String menuType) throws Exception {
        List<MenuDto> allMenuDtos = menuRepo.findByMenuType(menuType)
                .stream()
                .map(menu -> mapper.mapToDto(menu))
                .collect(Collectors.toList());
        Map<Long, NodeDto> nodeDtoMap = allMenuDtos.stream()
                .filter(menuDto -> menuDto.getRootMenuId() == ApplicationConst.app_root_menu_id)
                .collect(Collectors.toMap(menu -> menu.getId(), menu -> mapper.toNodeDto(menu, true, false)));
        allMenuDtos.stream()
                .filter(allMenuDto -> allMenuDto.getRootMenuId() != ApplicationConst.app_root_menu_id)
                .forEach(allMenuDto -> {
                    nodeDtoMap.get(allMenuDto.getRootMenuId()).setExpanded(true);
                    nodeDtoMap.get(allMenuDto.getRootMenuId()).getChildren()
                            .add(mapper.toNodeDto(allMenuDto, true, false));
                });
        return nodeDtoMap.values().stream().collect(Collectors.toList());
    }


    @Override
    public MenuDto create(MenuDto dto) throws Exception {
        return mapper.mapToDto(menuRepo.save(mapper.mapToModel(dto)));
    }

    @Override
    public MenuDto update(MenuDto dto) throws Exception {
        return mapper.mapToDto(menuRepo.findById(dto.getId())
                .map(model -> {
                    BeanUtils.copyProperties(dto, model);
                    menuRepo.save(model);
                    return model;
                }).get());
    }

    @Override
    public PaginationDto readAll(PaginationDto dto) throws Exception {
        Page<Menu> menuPage;
        if (StringUtils.hasText(dto.getSearchField()) && StringUtils.hasText(dto.getSearchValue())) {
            menuPage = menuRepo.findAll(menuRepo.searchSpec(dto.getSearchField(), dto.getSearchValue()),
                    StaticUtils.generatePageRequest.apply(dto));
        } else {
            menuPage = menuRepo.findAll(StaticUtils.generatePageRequest.apply(dto));
        }
        dto.setTotal(menuPage.getTotalElements());
        dto.setLoading(false);
        dto.setResult(menuPage.stream()
                .map(entity -> mapper.mapToDto(entity))
                .collect(Collectors.toList()));
        return dto;
    }

    @Override
    public MenuDto read(Long id) throws Exception {
        return
                mapper.mapToDto(menuRepo.findByIdAndStatus(id, ApplicationConst.active, Menu.class)
                        .orElseThrow(() -> new APPException("app.error.msg-17")));
    }

    @Override
    public Boolean delete(Long id) throws Exception {
        menuRepo.deleteById(id);
        return true;
    }

    @Override
    public Boolean changeStatus(Long id, boolean status) {
        return menuRepo.findById(id)
                .map(menu -> {
                    menu.setStatus(status);
                    menuRepo.save(menu);
                    return true;
                }).orElseThrow(() -> new APPException("app.error.msg-17"));

    }

    @Override
    public Map<Long, String> getIdAndNameMap() throws Exception {
        return menuRepo.findAllByStatus(ApplicationConst.active, Menu.class).stream()
                .collect(Collectors.toMap(Menu::getId, Menu::getName));
    }
}
