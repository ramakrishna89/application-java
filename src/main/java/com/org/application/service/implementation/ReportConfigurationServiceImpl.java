package com.org.application.service.implementation;

import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.stream.Collectors;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.PaginationDto;
import com.org.application.exception.APPException;
import com.org.application.utils.StaticUtils;
import com.org.application.model.ReportConfiguration;
import com.org.application.dto.ReportConfigurationDto;
import com.org.application.repository.ReportConfigurationRepo;
import com.org.application.service.interfaces.ReportConfigurationService;
import com.org.application.service.mapper.ReportConfigurationMapper;

@Service
@AllArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class ReportConfigurationServiceImpl implements ReportConfigurationService {
    private final ReportConfigurationMapper reportConfigurationMapper;
    private final ReportConfigurationRepo reportConfigurationRepo;

    @Override
    public ReportConfigurationDto create(ReportConfigurationDto dto) throws Exception {
        ReportConfiguration reportConfiguration = reportConfigurationMapper.toModel(dto);
        reportConfigurationRepo.save(reportConfiguration);
        return reportConfigurationMapper.toDto(reportConfiguration);
    }

    @Override
    public ReportConfigurationDto update(ReportConfigurationDto dto) throws Exception {
        return reportConfigurationMapper.toDto(reportConfigurationRepo.findById(dto.getId()).map(model -> {
            BeanUtils.copyProperties(dto, model);
            reportConfigurationRepo.save(model);
            return model;
        }).orElseThrow(() -> new APPException("app.error.msg - 17")));
    }

    @Override
    public PaginationDto readAll(PaginationDto dto) throws Exception {
        Page<ReportConfiguration> page;
        if (StringUtils.hasText(dto.getSearchField()) && StringUtils.hasText(dto.getSearchValue())) {
            page = reportConfigurationRepo.findAll(reportConfigurationRepo.searchSpec(dto.getSearchField(),
                    dto.getSearchValue()), StaticUtils.generatePageRequest.apply(dto));
        } else {
            page = reportConfigurationRepo.findAll(StaticUtils.generatePageRequest.apply(dto));
        }
        dto.setTotal(page.getTotalElements());
        dto.setLoading(false);
        dto.setResult(page.stream().map(entity -> reportConfigurationMapper.toDto(entity))
                .collect(Collectors.toList()));
        return dto;
    }

    @Override
    public ReportConfigurationDto read(Long id) throws Exception {
        return reportConfigurationMapper.toDto(reportConfigurationRepo.findByIdAndStatus(id, ApplicationConst.active,
                ReportConfiguration.class).orElseThrow(() -> new APPException("app.error.msg - 17")));
    }

    @Override
    public Boolean delete(Long id) throws Exception {
        reportConfigurationRepo.deleteById(id);
        return true;
    }

    @Override
    public Boolean changeStatus(Long id, boolean status) {
        return reportConfigurationRepo.findById(id).map(object -> {
            object.setStatus(status);
            reportConfigurationRepo.save(object);
            return true;
        }).orElseThrow(() -> new APPException("app.error.msg - 17"));
    }

    @Override
    public Map<Long, String> getIdAndNameMap() throws Exception {
        return reportConfigurationRepo.findAllByStatus(ApplicationConst.active, ReportConfiguration.class).stream().collect(Collectors.toMap(ReportConfiguration::getId, ReportConfiguration::getName));
    }
}
