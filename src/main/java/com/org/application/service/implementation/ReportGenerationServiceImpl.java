package com.org.application.service.implementation;

import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.stream.Collectors;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.PaginationDto;
import com.org.application.exception.APPException;
import com.org.application.utils.StaticUtils;
import com.org.application.model.ReportGeneration;
import com.org.application.dto.ReportGenerationDto;
import com.org.application.repository.ReportGenerationRepo;
import com.org.application.service.interfaces.ReportGenerationService;
import com.org.application.service.mapper.ReportGenerationMapper;

@Service
@AllArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class ReportGenerationServiceImpl implements ReportGenerationService {
    private final ReportGenerationMapper reportGenerationMapper;
    private final ReportGenerationRepo reportGenerationRepo;

    @Override
    public ReportGenerationDto create(ReportGenerationDto dto) throws Exception {
        ReportGeneration reportGeneration = reportGenerationMapper.toModel(dto);
        reportGenerationRepo.save(reportGeneration);
        return reportGenerationMapper.toDto(reportGeneration);
    }

    @Override
    public ReportGenerationDto update(ReportGenerationDto dto) throws Exception {
        return reportGenerationMapper.toDto(reportGenerationRepo.findById(dto.getId()).map(model -> {
            BeanUtils.copyProperties(dto, model);
            reportGenerationRepo.save(model);
            return model;
        }).orElseThrow(() -> new APPException("app.error.msg - 17")));
    }

    @Override
    public PaginationDto readAll(PaginationDto dto) throws Exception {
        Page<ReportGeneration> page;
        if (StringUtils.hasText(dto.getSearchField()) && StringUtils.hasText(dto.getSearchValue())) {
            page = reportGenerationRepo.findAll(reportGenerationRepo.searchSpec(dto.getSearchField(),
                    dto.getSearchValue()), StaticUtils.generatePageRequest.apply(dto));
        } else {
            page = reportGenerationRepo.findAll(StaticUtils.generatePageRequest.apply(dto));
        }
        dto.setTotal(page.getTotalElements());
        dto.setLoading(false);
        dto.setResult(page.stream().map(entity -> reportGenerationMapper.toDto(entity)).collect(Collectors.toList()));
        return dto;
    }

    @Override
    public ReportGenerationDto read(Long id) throws Exception {
        return reportGenerationMapper.toDto(reportGenerationRepo.findByIdAndStatus(id,
                        ApplicationConst.active, ReportGeneration.class)
                .orElseThrow(() -> new APPException("app.error.msg - 17")));
    }

    @Override
    public Boolean delete(Long id) throws Exception {
        reportGenerationRepo.deleteById(id);
        return true;
    }

    @Override
    public Boolean changeStatus(Long id, boolean status) {
        return reportGenerationRepo.findById(id).map(object -> {
            object.setStatus(status);
            reportGenerationRepo.save(object);
            return true;
        }).orElseThrow(() -> new APPException("app.error.msg - 17"));
    }

    @Override
    public Map<Long, String> getIdAndNameMap() throws Exception {
        return reportGenerationRepo.findAllByStatus(ApplicationConst.active, ReportGeneration.class)
                .stream().collect(Collectors.toMap(ReportGeneration::getId, ReportGeneration::getName));
    }
}
