package com.org.application.service.implementation;


import com.org.application.constants.ApplicationConst;
import com.org.application.dto.AuthDto;
import com.org.application.exception.APPException;
import com.org.application.model.RefreshToken;
import com.org.application.model.User;
import com.org.application.props.ApplicationProps;
import com.org.application.repository.RefreshTokenRepo;
import com.org.application.repository.RoleAccessRepo;
import com.org.application.repository.UserRepo;
import com.org.application.service.interfaces.AuthenticationService;
import com.org.application.service.mapper.RoleAccessMapper;
import com.org.application.service.mapper.UserMapper;
import com.org.application.service.security.JWTProvider;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@AllArgsConstructor
@Transactional
public class AuthenticationServiceImpl implements AuthenticationService {


    private final UserRepo userRepo;
    private final RoleAccessRepo roleAccessRepo;
    private final UserMapper userMapper;
    private final RoleAccessMapper roleAccessMapper;
    private final RefreshTokenRepo refreshTokenRepo;
    private final AuthenticationManager authenticationManager;
    private final JWTProvider jwtProvider;
    private final ApplicationProps props;


    @Override
    public Long checkAdmin() throws Exception {
        return roleAccessRepo.countByNameAndDescription(ApplicationConst.app_role_sysadmin_code,
                ApplicationConst.app_role_sysadmin).orElseThrow(() -> new APPException("app.error.msg-3"));
    }


    @Override
    public AuthDto authenticateUser(AuthDto dto) throws Exception {
        Authentication authentication =
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                        dto.getEmail(),
                        dto.getPassword()));

        RefreshToken token = RefreshToken.builder()
                .token(UUID.randomUUID().toString())
                .build();
        refreshTokenRepo.save(token);

        dto.setRefreshToken(token.getToken());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        jwtProvider.generateToken(authentication, dto);
        User user = userRepo.findByEmail(dto.getEmail()).get();
        return userMapper.mapToAuthDto(dto, user, roleAccessMapper.toDto(user.getRoleAccess()),
                props.getPermissionMode());
    }

    @Override
    public AuthDto refreshToken(AuthDto dto) throws Exception {
        RefreshToken refreshToken = refreshTokenRepo.findByToken(
                dto.getRefreshToken()).orElseThrow(
                () -> new APPException("app.error.msg-29"));
        User user = userRepo.findByEmail(dto.getEmail()).get();
        userMapper.mapToAuthDto(dto, user, roleAccessMapper.toDto(user.getRoleAccess()),
                props.getPermissionMode());
        return jwtProvider.refreshJWTToken(dto, refreshToken.getToken());
    }

    @Override
    public void deleteAuthRefreshToken(String token) throws Exception {
        refreshTokenRepo.deleteByToken(token);
    }

}
