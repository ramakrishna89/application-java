package com.org.application.service.implementation;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.NotificationEmailDto;
import com.org.application.dto.UserDto;
import com.org.application.exception.APPException;
import com.org.application.model.*;
import com.org.application.props.ApplicationProps;
import com.org.application.repository.*;
import com.org.application.service.component.MailerComponent;
import com.org.application.service.interfaces.RegistrationService;
import com.org.application.service.mapper.UserMapper;
import com.org.application.utils.DIUtils;
import com.org.application.utils.StaticUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.chrono.ChronoLocalDateTime;

@Service
@Transactional
@AllArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {

    private final UserRepo userRepo;
    private final RoleAccessRepo roleAccessRepo;
    private final RoleDataRepo roleDataRepo;
    private final ProductRepo productRepo;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final MailerComponent mailerComponent;
    private final MenuRepo menuRepo;
    private final PasswordRepo passwordRepo;
    private final VerificationTokenRepo verificationTokenRepo;
    private final ApplicationProps props;
    private final DIUtils diUtils;

    @Override
    public UserDto sysAdminSignUp(UserDto dto) throws Exception {

        userRepo.findByEmail(dto.getEmail())
                .ifPresent(existingUser -> {
                    throw new APPException("app.error.msg-35");
                });


        Product product = Product.builder()
                .name(ApplicationConst.app_base_product_code)
                .description(ApplicationConst.app_base_product)
                .code(ApplicationConst.app_base_product_code)
                .status(ApplicationConst.active)
                .build();
        productRepo.save(product);

        RoleData roleData = RoleData.builder()
                .name(ApplicationConst.app_role_sysadmin_code)
                .code(ApplicationConst.app_role_sysadmin_code)
                .description(ApplicationConst.app_role_sysadmin)
                .status(ApplicationConst.active)
                .build();
        roleDataRepo.save(roleData);

        RoleAccess roleAccess = RoleAccess.builder()
                .name(ApplicationConst.app_role_sysadmin_code)
                .description(ApplicationConst.app_role_sysadmin)
                .permission(props.getPermissionMode())
                .status(ApplicationConst.active)
                .menus(menuRepo.findAll())
                .roleData(roleData)
                .product(product)
                .build();
        roleAccessRepo.save(roleAccess);

        String tempPassword = diUtils.generatePassay();
        Password password = Password.builder()
                .password(passwordEncoder.encode(tempPassword))
                .build();
        passwordRepo.save(password);

        User user = userMapper.toModel(dto);
        user.setPassword(password);
        user.setRoleAccess(roleAccess);
        userRepo.save(user);

        String token = StaticUtils.generateUUID();
        VerificationToken verificationToken = VerificationToken.builder()
                .token(token)
                .user(user)
                .build();
        verificationTokenRepo.save(verificationToken);

        NotificationEmailDto emailDto = NotificationEmailDto.builder()
                .type(ApplicationConst.app_mail_activation)
                .recipient(user.getEmail())
                .activationUrl((props.getDomainURL() + props.getActivationLink() + token))
                .tempPassword(tempPassword).build();
        mailerComponent.sendMail(emailDto);
        BeanUtils.copyProperties(user, dto);
        return dto;
    }

    @Override
    public String generateVerificationToken(String email) throws Exception {
        VerificationToken model = null;
        String token = null;
        User user = null;
        try {
            user = userRepo.findByEmail(email).orElseThrow(() -> new APPException("app.error.msg-8"));
            model = new VerificationToken();
            token = StaticUtils.generateUUID();
            model.setToken(token);
            model.setUser(user);
            verificationTokenRepo.save(model);

            String tempPassword = diUtils.generatePassay();
            user.getPassword().setPassword(passwordEncoder.encode(tempPassword));
            passwordRepo.save(user.getPassword());

            user.setStatus(false);
            userRepo.save(user);


            NotificationEmailDto emailDto = NotificationEmailDto.builder()
                    .type(ApplicationConst.app_mail_activation)
                    .recipient(user.getEmail())
                    .activationUrl((props.getDomainURL() + props.getActivationLink() + token))
                    .tempPassword(tempPassword).build();
            mailerComponent.sendMail(emailDto);
            return token;
        } finally {
        }
    }

    @Override
    public void accountActivation(String token) throws Exception {
        VerificationToken tokenModel = null;
        verificationTokenRepo.findByToken(token)
                .map(verificationToken -> {
                    if (verificationToken.getCreatedAt().plusHours(props.getActivationMailDurationInHour())
                            .isAfter(LocalDateTime.now())) {
                        return userRepo.findById(verificationToken.getUser().getId())
                                .map(user -> {
                                    user.setStatus(true);
                                    userRepo.save(user);
                                    return verificationToken;
                                }).orElseThrow(() -> new APPException("app.error.msg-8"));
                    } else {
                        throw new APPException("app.error.msg-36");
                    }
                }).orElseThrow(() -> new APPException("app.error.msg-8"));
    }

}
