package com.org.application.service.implementation;


import com.org.application.dto.PaginationDto;
import com.org.application.dto.PasswordDto;
import com.org.application.exception.APPException;
import com.org.application.repository.PasswordRepo;
import com.org.application.repository.UserRepo;
import com.org.application.service.interfaces.PasswordService;
import com.org.application.service.mapper.PasswordMapper;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@AllArgsConstructor
@Transactional
public class PasswordServiceImpl implements PasswordService {

    private final PasswordRepo repo;
    private final UserRepo userRepo;
    private final PasswordMapper mapper;
    private final PasswordEncoder passwordEncoder;


    @Override
    public PasswordDto create(PasswordDto dto) throws Exception {
        dto.setPassword(passwordEncoder.encode(dto.getPassword()));
        return mapper.toDto(repo.save(mapper.toModel(dto)));
    }

    @Override
    public PasswordDto update(PasswordDto dto) throws Exception {
        return userRepo.findByEmail(dto.getEmail())
                .map(model -> {
                    if (passwordEncoder.matches(dto.getOldPassword(), model.getPassword().getPassword())) {
                        model.getPassword().setPassword(passwordEncoder.encode(dto.getPassword()));
                        repo.save(model.getPassword());
                        return dto;
                    } else {
                        throw new APPException("app.error.msg-26");
                    }
                }).orElseThrow(() -> new APPException("app.error.msg-8"));
    }

    @Override
    public PaginationDto readAll(PaginationDto dto) throws Exception {
        return null;
    }

    @Override
    public PasswordDto read(Long id) throws Exception {
        return null;
    }

    @Override
    public Boolean delete(Long id) throws Exception {
        return null;
    }

    @Override
    public Boolean changeStatus(Long id, boolean status) throws Exception {
        return null;
    }

    @Override
    public Map<Long, String> getIdAndNameMap() throws Exception {
        return null;
    }
}
