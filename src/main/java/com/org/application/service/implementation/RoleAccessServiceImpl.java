package com.org.application.service.implementation;


import com.org.application.constants.ApplicationConst;
import com.org.application.dto.MenuDto;
import com.org.application.dto.PaginationDto;
import com.org.application.dto.RoleAccessDto;
import com.org.application.exception.APPException;
import com.org.application.model.Menu;
import com.org.application.model.Product;
import com.org.application.model.RoleAccess;
import com.org.application.model.RoleData;
import com.org.application.props.ApplicationProps;
import com.org.application.repository.MenuRepo;
import com.org.application.repository.ProductRepo;
import com.org.application.repository.RoleAccessRepo;
import com.org.application.repository.RoleDataRepo;
import com.org.application.service.interfaces.RoleAccessService;
import com.org.application.service.mapper.MenuMapper;
import com.org.application.service.mapper.RoleAccessMapper;
import com.org.application.utils.StaticUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class RoleAccessServiceImpl implements RoleAccessService {

    private final RoleAccessRepo roleAccessRepo;
    private final MenuRepo menuRepo;
    private final RoleAccessMapper roleAccessMapper;
    private final MenuMapper menuMapper;
    private final RoleDataRepo roleDataRepo;
    private final ProductRepo productRepo;
    private final ApplicationProps applicationProps;


    @Override
    public RoleAccessDto create(RoleAccessDto dto) throws Exception {
        return roleAccessMapper.toDto(roleAccessRepo.save(RoleAccess.builder()
                .name(dto.getName())
                .description(dto.getDescription())
                .permission(applicationProps.getPermissionMode())
                .status(dto.isStatus())
                .menus(dto.getSelectedMenuIds().stream().map(id -> menuRepo.findById(id)
                                .orElseThrow(() -> new APPException("app.error.msg-17")))
                        .collect(Collectors.toList()))
                .roleDataList(dto.getSelectedRoleDataIds().stream().map(id -> roleDataRepo.findById(id)
                        .orElseThrow(() -> new APPException("app.error.msg-31"))).collect(Collectors.toList()))
                .productList(dto.getSelectedProductIds().stream().map(id -> productRepo.findById(id)
                        .orElseThrow(() -> new APPException("app.error.msg-38"))).collect(Collectors.toList()))
                .build()));
    }

    @Override
    public RoleAccessDto update(RoleAccessDto dto) throws Exception {
        return roleAccessRepo.findById(dto.getId())
                .map(model -> {
                    roleAccessRepo.save(RoleAccess.builder()
                            .id(dto.getId())
                            .name(dto.getName())
                            .description(dto.getDescription())
                            .permission(applicationProps.getPermissionMode())
                            .status(dto.isStatus())
                            .menus(dto.getSelectedMenuIds().stream().map(id -> menuRepo.findById(id)
                                            .orElseThrow(() -> new APPException("app.error.msg-17")))
                                    .collect(Collectors.toList()))
                            .roleDataList(dto.getSelectedRoleDataIds().stream().map(id -> roleDataRepo.findById(id)
                                            .orElseThrow(() -> new APPException("app.error.msg-31")))
                                    .collect(Collectors.toList()))
                            .productList(dto.getSelectedProductIds().stream().map(id -> productRepo.findById(id)
                                    .orElseThrow(() -> new APPException("app.error.msg-38"))).collect(Collectors.toList()))
                            .build());
                    return dto;
                }).orElseThrow(() -> new APPException("app.error.msg-8"));
    }

    @Override
    public PaginationDto readAll(PaginationDto dto) throws Exception {
        Page<RoleAccess> page;
        if (StringUtils.hasText(dto.getSearchField()) && StringUtils.hasText(dto.getSearchValue())) {
            page = roleAccessRepo.findAll(roleAccessRepo.searchSpec(dto.getSearchField(), dto.getSearchValue()),
                    StaticUtils.generatePageRequest.apply(dto));
        } else {
            page = roleAccessRepo.findAll(StaticUtils.generatePageRequest.apply(dto));
        }
        dto.setTotal(page.getTotalElements());
        dto.setLoading(false);
        dto.setResult(page.stream()
                .map(entity -> roleAccessMapper.toDto(entity))
                .collect(Collectors.toList()));
        return dto;
    }

    @Override
    public RoleAccessDto read(Long id) throws Exception {
        return
                roleAccessRepo.findByIdAndStatus(id, ApplicationConst.active, RoleAccess.class)
                        .map(model -> {
                            RoleAccessDto dto = roleAccessMapper.toDto(model);
                            dto.setSelectedMenuIds(model.getMenus().stream().map(Menu::getId)
                                    .collect(Collectors.toList()));
                            dto.setSelectedRoleDataIds(model.getRoleDataList().stream().map(RoleData::getId)
                                    .collect(Collectors.toList()));
                            dto.setSelectedProductIds(model.getProductList().stream().map(Product::getId)
                                    .collect(Collectors.toList()));
                            return dto;
                        })
                        .orElseThrow(() -> new APPException("app.error.msg-17"));
    }

    @Override
    public Boolean delete(Long id) throws Exception {
        roleAccessRepo.deleteById(id);
        return true;
    }

    @Override
    public Boolean changeStatus(Long id, boolean status) {
        return roleAccessRepo.findById(id)
                .map(object -> {
                    object.setStatus(status);
                    roleAccessRepo.save(object);
                    return true;
                }).orElseThrow(() -> new APPException("app.error.msg-17"));

    }

    @Override
    @GetMapping(ApplicationConst.app_nav_IdNameMap)
    public Map<Long, String> getIdAndNameMap() throws Exception {
        return roleAccessRepo.findAllByStatus(ApplicationConst.active, RoleAccess.class).stream()
                .collect(Collectors.toMap(RoleAccess::getId, RoleAccess::getName));
    }

    @Override
    public List<MenuDto> loadMenusOfRoleAccess(Long roleId) throws Exception {
        List<MenuDto> allMenuDtos = roleAccessRepo.findById(roleId)
                .orElseThrow(() -> new APPException("app.error.msg-30"))
                .getMenus().stream()
                .map(menu -> menuMapper.mapToDto(menu))
                .collect(Collectors.toList());
        Map<Long, MenuDto> rootMenuDtos = allMenuDtos.stream()
                .filter(menuDto -> menuDto.getRootMenuId() == ApplicationConst.app_root_menu_id)
                .collect(Collectors.toMap(MenuDto::getId, Function.identity()));
        allMenuDtos.stream()
                .filter(allMenuDto -> allMenuDto.getRootMenuId() != ApplicationConst.app_root_menu_id)
                .forEach(allMenuDto -> rootMenuDtos.get(allMenuDto.getRootMenuId()).getSubMenuDtos().add(allMenuDto));
        return rootMenuDtos.values().stream().sorted().collect(Collectors.toList());
    }
}
