package com.org.application.service.implementation;

import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.stream.Collectors;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.PaginationDto;
import com.org.application.exception.APPException;
import com.org.application.utils.StaticUtils;
import com.org.application.model.Product;
import com.org.application.dto.ProductDto;
import com.org.application.repository.ProductRepo;
import com.org.application.service.interfaces.ProductService;
import com.org.application.service.mapper.ProductMapper;

@Service
@AllArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class ProductServiceImpl implements ProductService {
    private final ProductMapper productMapper;
    private final ProductRepo productRepo;

    @Override
    public ProductDto create(ProductDto dto) throws Exception {
        Product product = productMapper.toModel(dto);
        productRepo.save(product);
        return productMapper.toDto(product);
    }

    @Override
    public ProductDto update(ProductDto dto) throws Exception {
        return productMapper.toDto(productRepo.findById(dto.getId()).map(model -> {
            BeanUtils.copyProperties(dto, model);
            productRepo.save(model);
            return model;
        }).orElseThrow(() -> new APPException("app.error.msg - 17")));
    }

    @Override
    public PaginationDto readAll(PaginationDto dto) throws Exception {
        Page<Product> page;
        if (StringUtils.hasText(dto.getSearchField()) && StringUtils.hasText(dto.getSearchValue())) {
            page = productRepo.findAll(productRepo.searchSpec(dto.getSearchField(), dto.getSearchValue()),
                    StaticUtils.generatePageRequest.apply(dto));
        } else {
            page = productRepo.findAll(StaticUtils.generatePageRequest.apply(dto));
        }
        dto.setTotal(page.getTotalElements());
        dto.setLoading(false);
        dto.setResult(page.stream().map(entity -> productMapper.toDto(entity)).collect(Collectors.toList()));
        return dto;
    }

    @Override
    public ProductDto read(Long id) throws Exception {
        return productMapper.toDto(productRepo.findByIdAndStatus(id, ApplicationConst.active, Product.class)
                .orElseThrow(() -> new APPException("app.error.msg - 17")));
    }

    @Override
    public Boolean delete(Long id) throws Exception {
        productRepo.deleteById(id);
        return true;
    }

    @Override
    public Boolean changeStatus(Long id, boolean status) {
        return productRepo.findById(id).map(object -> {
            object.setStatus(status);
            productRepo.save(object);
            return true;
        }).orElseThrow(() -> new APPException("app.error.msg - 17"));
    }

    @Override
    public Map<Long, String> getIdAndNameMap() throws Exception {
        return productRepo.findAllByStatus(ApplicationConst.active, Product.class)
                .stream().collect(Collectors.toMap(Product::getId, Product::getName));
    }
}
