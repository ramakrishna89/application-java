package com.org.application.service.security;

import com.org.application.constants.ApplicationConst;
import com.org.application.service.security.JWTProvider;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
@AllArgsConstructor
public class JWTAuthenticationFilter extends OncePerRequestFilter {

    private final JWTProvider jwtProvider;
    private final UserDetailsService userDetailsService;

    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String token = null, username = null;
        UserDetails userDetails = null;
        UsernamePasswordAuthenticationToken upaToken = null;
        try {
            token = getToken(httpServletRequest);
            if (StringUtils.hasText(token)) {
                // username = jwtProvider.getSubjectFromToken(token);
                username = jwtProvider.getSubjectAndValidateToken(token);
                if (StringUtils.hasText(username)) {
                    userDetails = userDetailsService.loadUserByUsername(username);
                    upaToken = new UsernamePasswordAuthenticationToken(userDetails, null,
                            userDetails.getAuthorities());
                    upaToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    SecurityContextHolder.getContext().setAuthentication(upaToken);
                }
            }
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } finally {
        }
    }

    private String getToken(HttpServletRequest request) throws Exception {
        String bearerToken = null;
        try {
            bearerToken = request.getHeader(ApplicationConst.app_jwt_header_name);
            if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(ApplicationConst.app_jwt_bearer_text)) {
                return bearerToken.substring(7);
            }
            return bearerToken;
        } finally {
            bearerToken = null;
        }
    }
}
