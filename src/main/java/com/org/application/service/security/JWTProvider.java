package com.org.application.service.security;


import com.org.application.dto.AuthDto;
import com.org.application.props.ApplicationProps;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

@Service
@Slf4j
public class JWTProvider {

    private KeyStore keyStore;
    private final ApplicationProps props;

    public JWTProvider(ApplicationProps props) {
        this.props = props;
    }

    @PostConstruct
    public void init() throws Exception {
        InputStream inputStream = null;
        try {
            keyStore = KeyStore.getInstance("JKS");
            inputStream = getClass().getResourceAsStream("/" + props.getKeystoreName());
            keyStore.load(inputStream, props.getKeystorePassword().toCharArray());
        } finally {
        }
    }

    public AuthDto generateToken(Authentication authentication, AuthDto dto) throws Exception {
        UserDetailsImpl principal = null;
        try {
            principal = (UserDetailsImpl) authentication.getPrincipal();
            dto.setPassword(null);
            dto.setExpiresAt(LocalDateTime.now().plusMinutes(
                    props.getJwtTimeoutInMinutes()));
            dto.setToken(Jwts.builder()
                    .setSubject(principal.getUsername())
                    .setIssuedAt(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()))
                    .signWith(getPrivateKey())
                    .setExpiration(Date.from(dto.getExpiresAt().atZone(ZoneId.systemDefault()).toInstant()))
                    .compact());
            return dto;
        } finally {
        }
    }

    public AuthDto refreshJWTToken(AuthDto dto, String refreshToken) throws Exception {
        try {
            dto.setRefreshToken(refreshToken);
            dto.setPassword(null);
            dto.setExpiresAt(LocalDateTime.now().plusMinutes(
                    props.getJwtTimeoutInMinutes()));
            dto.setToken(Jwts.builder()
                    .setSubject(dto.getEmail())
                    .setIssuedAt(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()))
                    .signWith(getPrivateKey())
                    .setExpiration(Date.from(dto.getExpiresAt().atZone(ZoneId.systemDefault()).toInstant()))
                    .compact());
            return dto;
        } finally {
        }
    }

    private PrivateKey getPrivateKey() throws Exception {
        return (PrivateKey) keyStore.getKey(props.getKeyAlias(),
                props.getKeystorePassword().toCharArray());
    }

    public String getSubjectFromToken(String token) throws Exception {
        return Jwts.parserBuilder().setSigningKey(getPrivateKey())
                .build().parseClaimsJws(token).getBody().getSubject();
    }

    public String getSubjectAndValidateToken(String token) {
        try {
            Claims obj = Jwts.parserBuilder().setSigningKey(getPublicKey()).build()
                    .parseClaimsJws(token).getBody();
            if (StringUtils.hasText(obj.getSubject()) && !obj.getExpiration()
                    .before(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()))) {
                return obj.getSubject();
            }
        } catch (Exception ex) {
            log.info("Error: " + ex);
        }
        return null;
    }

    private PublicKey getPublicKey() throws Exception {
        return keyStore.getCertificate(props.getKeyAlias()).getPublicKey();
    }
}
