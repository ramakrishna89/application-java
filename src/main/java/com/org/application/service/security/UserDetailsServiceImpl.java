package com.org.application.service.security;

import com.org.application.model.User;
import com.org.application.repository.MenuRepo;
import com.org.application.repository.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepo userRepo;
    private final MenuRepo menuRepo;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepo.findByEmail(email).orElseThrow(
                () -> new UsernameNotFoundException("Email id Not Found."));
        return UserDetailsImpl.builder()
                .id(user.getId())
                .username(user.getEmail())
                .actualName(user.getName())
                .password(user.getPassword().getPassword())
                .enabled(user.isStatus())
                .accountNonExpired(true)
                .credentialsNonExpired(true)
                .accountNonLocked(true)
                .authorities(getAuthorities())
                .build();
    }


    private Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorityList = new ArrayList<SimpleGrantedAuthority>();
        menuRepo.findDistinctMenuType().stream()
                .forEach(menuType -> authorityList.add(new SimpleGrantedAuthority(menuType)));
        return authorityList;
    }
}
