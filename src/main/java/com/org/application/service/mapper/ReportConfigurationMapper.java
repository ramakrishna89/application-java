package com.org.application.service.mapper;

import com.org.application.dto.ReportConfigurationDto;
import com.org.application.model.ReportConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReportConfigurationMapper {
    ReportConfigurationDto toDto(ReportConfiguration model);

    ReportConfiguration toModel(ReportConfigurationDto dto);
}
