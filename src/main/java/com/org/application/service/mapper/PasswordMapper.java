package com.org.application.service.mapper;

import com.org.application.dto.PasswordDto;
import com.org.application.model.Password;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PasswordMapper {

    PasswordDto toDto(Password Password);

    Password toModel(PasswordDto dto);

    List<PasswordDto> toDtoList(List<Password> PasswordList);
}
