package com.org.application.service.mapper;

import com.org.application.dto.RoleAccessDto;
import com.org.application.model.RoleAccess;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoleAccessMapper {

    RoleAccessDto toDto(RoleAccess roleAccess);

    RoleAccess toModel(RoleAccessDto dto);

    List<RoleAccessDto> toDtoList(List<RoleAccess> roleAccessList);
}
