package com.org.application.service.mapper;

import com.org.application.dto.ReportGenerationDto;
import com.org.application.model.ReportGeneration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReportGenerationMapper {
    ReportGenerationDto toDto(ReportGeneration model);

    ReportGeneration toModel(ReportGenerationDto dto);
}
