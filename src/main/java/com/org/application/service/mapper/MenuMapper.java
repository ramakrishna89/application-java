package com.org.application.service.mapper;

import com.org.application.dto.MenuDto;
import com.org.application.dto.NodeDto;
import com.org.application.model.Menu;
import org.mapstruct.Mapper;

import javax.validation.constraints.NotNull;
import java.util.List;

@Mapper(componentModel = "spring")
public interface MenuMapper {

    MenuDto mapToDto(Menu entity);

    Menu mapToModel(MenuDto dto);

    List<MenuDto> mapToDtoList(List<Menu> menuList);

    default NodeDto toNodeDto(@NotNull MenuDto dto, boolean isLeaf, boolean isChecked) {
        return NodeDto.builder()
                .key(dto.getId())
                .parentKey(dto.getRootMenuId())
                .title(dto.getPath())
                .expanded(false)
                .selectable(false)
                .isLeaf(isLeaf)
                .checked(isChecked)
                .build();
    }
}
