package com.org.application.service.mapper;

import com.org.application.dto.RoleDataDto;
import com.org.application.model.RoleData;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoleDataMapper {

    RoleData toModel(RoleDataDto dto);

    RoleDataDto toDto(RoleData entity);

    List<RoleDataDto> toDto(List<RoleData> roleData);
}
