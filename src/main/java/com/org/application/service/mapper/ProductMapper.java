package com.org.application.service.mapper;

import com.org.application.dto.ProductDto;
import com.org.application.model.Product;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductDto toDto(Product model);

    Product toModel(ProductDto dto);
}
