package com.org.application.service.mapper;

import com.org.application.dto.AuthDto;
import com.org.application.dto.RoleAccessDto;
import com.org.application.dto.UserDto;
import com.org.application.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import javax.validation.constraints.NotNull;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(source = "user.email", target = "email")
    @Mapping(source = "user.roleAccess.id", target = "selectedRoleAccessId")
    UserDto toDto(User user);

    User toModel(UserDto dto);

    default AuthDto mapToAuthDto(@NotNull AuthDto authDto, @NotNull User user, RoleAccessDto roleAccessDto,
                                 Integer appPermissionMode) {
        authDto.setAppPermission(appPermissionMode);
        authDto.setName(user.getName());
        authDto.setRoleAccessDto(roleAccessDto);
        return authDto;
    }
}
