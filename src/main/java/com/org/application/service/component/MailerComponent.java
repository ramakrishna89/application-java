package com.org.application.service.component;


import com.org.application.constants.ApplicationConst;
import com.org.application.dto.NotificationEmailDto;
import com.org.application.exception.APPException;
import com.org.application.props.ApplicationProps;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Calendar;

@Component
@AllArgsConstructor
@Slf4j
public class MailerComponent {

    private final TemplateEngine templateEngine;
    private final JavaMailSender javaMailSender;
    private final ApplicationProps props;

    @Async
    public void sendMail(NotificationEmailDto dto) {
        try {
            MimeMessagePreparator preparator = mimeMessage -> {
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
                helper.setFrom(props.getMailFrom());
                helper.setTo(dto.getRecipient());
                if (ApplicationConst.app_mail_activation == dto.getType()) {
                    helper.setSubject(props.getSubjectActivation());
                }
                helper.setText(build(dto), true);
            };
            javaMailSender.send(preparator);
            log.info("Activation mail sent.");
        } catch (Exception ex) {
            log.error("Notification Email error: " + ex);
            throw new APPException("Notification Email error: " + ex);
        } finally {
        }
    }


    public String build(NotificationEmailDto dto) throws Exception {
        Context ctx = null;
        try {
            ctx = new Context();
            loadDefaults(ctx);
            if (ApplicationConst.app_mail_activation == dto.getType()) {
                ctx.setVariable("subject", props.getSubjectActivation());
                ctx.setVariable("activationUrl", dto.getActivationUrl());
                ctx.setVariable("message", props.getActivationMessage());
                ctx.setVariable("subMessage", (props.getActivationSubMessage()
                        + dto.getTempPassword()));
            }
            return templateEngine.process("mailTemplate", ctx);
        } finally {
            ctx = null;
        }
    }

    private void loadDefaults(Context ctx) throws Exception {
        ctx.setVariable("name", props.getName());
        ctx.setVariable("date", Calendar.getInstance());
        ctx.setVariable("regards", props.getRegards());
        ctx.setVariable("sender", props.getSender());
        ctx.setVariable("activateBtn", props.getActivateBtn());
        ctx.setVariable("company", props.getCompany());
        ctx.setVariable("companyAddress", props.getCompanyAddress());
    }
}
