package com.org.application.service.interfaces;

import com.org.application.dto.UserDto;

public interface RegistrationService {

    UserDto sysAdminSignUp(UserDto dto) throws Exception;

    String generateVerificationToken(String email) throws Exception;

    void accountActivation(String token) throws Exception;

}
