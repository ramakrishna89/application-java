package com.org.application.service.interfaces;

import com.org.application.dto.MenuDto;
import com.org.application.dto.NodeDto;
import com.org.application.model.Menu;

import java.util.List;
import java.util.Set;

public interface MenuService extends GenericService<MenuDto> {

    List<MenuDto> loadMenuForRole(Set<Long> roleIds) throws Exception;

    List<String> loadAllMenuTypes() throws Exception;

    List<NodeDto> loadMenusOfMenuType(String menuType) throws Exception;

}
