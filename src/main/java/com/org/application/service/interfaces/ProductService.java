package com.org.application.service.interfaces;

import com.org.application.dto.ProductDto;

public interface ProductService extends GenericService<ProductDto> {
}
