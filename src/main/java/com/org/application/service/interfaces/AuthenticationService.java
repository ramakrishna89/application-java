package com.org.application.service.interfaces;


import com.org.application.dto.AuthDto;

public interface AuthenticationService {


    Long checkAdmin() throws Exception;

    AuthDto authenticateUser(AuthDto dto) throws Exception;

    AuthDto refreshToken(AuthDto dto) throws Exception;

    void deleteAuthRefreshToken(String token) throws Exception;

}
