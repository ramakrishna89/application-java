package com.org.application.service.interfaces;

import com.org.application.dto.RoleDataDto;
import com.org.application.model.RoleData;

public interface RoleDataService extends GenericService<RoleDataDto> {
}
