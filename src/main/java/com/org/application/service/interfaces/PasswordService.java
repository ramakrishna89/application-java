package com.org.application.service.interfaces;

import com.org.application.dto.PasswordDto;
import com.org.application.model.Password;

public interface PasswordService extends GenericService<PasswordDto> {
}
