package com.org.application.service.interfaces;

import com.org.application.dto.ReportGenerationDto;

public interface ReportGenerationService extends GenericService<ReportGenerationDto> {
}
