package com.org.application.service.interfaces;

import com.org.application.dto.MenuDto;
import com.org.application.dto.RoleAccessDto;
import com.org.application.model.RoleAccess;

import java.util.List;
import java.util.Set;

public interface RoleAccessService extends GenericService<RoleAccessDto> {

    List<MenuDto> loadMenusOfRoleAccess(Long roleId) throws Exception;

}
