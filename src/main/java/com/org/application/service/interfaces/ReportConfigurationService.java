package com.org.application.service.interfaces;

import com.org.application.dto.ReportConfigurationDto;

public interface ReportConfigurationService extends GenericService<ReportConfigurationDto> {
}
