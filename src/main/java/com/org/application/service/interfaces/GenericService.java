package com.org.application.service.interfaces;

import com.org.application.dto.PaginationDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import java.util.Map;

public interface GenericService<T> {

    T create(T dto) throws Exception;

    T update(T dto) throws Exception;

    PaginationDto readAll(PaginationDto dto) throws Exception;

    T read(Long id) throws Exception;

    Boolean delete(Long id) throws Exception;

    Boolean changeStatus(Long id, boolean status) throws Exception;

    Map<Long, String> getIdAndNameMap() throws Exception;


}
