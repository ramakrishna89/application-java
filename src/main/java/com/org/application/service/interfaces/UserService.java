package com.org.application.service.interfaces;

import com.org.application.dto.UserDto;

public interface UserService extends GenericService<UserDto> {

    Boolean resendActivationMail(long id) throws Exception;
}
