package com.org.application.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AuthDto implements Serializable {

    @NotBlank(message = "Email is required")
    private String email;
    private String password;
    private String token;
    private String refreshToken;
    private LocalDateTime expiresAt;
    private String name;
    private Integer permission;
    private Integer appPermission;
    private RoleAccessDto roleAccessDto;

}
