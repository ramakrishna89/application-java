package com.org.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageSearchDto implements Serializable {

    private String key;
    private String value;
    private boolean visibility;
}
