package com.org.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class RoleAccessDto extends GenericDto implements Serializable {

    private List<Long> selectedMenuIds;
    private List<Long> selectedRoleDataIds;
    private List<Long> selectedProductIds;


}
