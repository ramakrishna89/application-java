package com.org.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class APIResponseDto implements Serializable {

    private boolean status;
    private String message;
    private Integer type;
    private Object object;
}
