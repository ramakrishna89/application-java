package com.org.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString
public class DBDetailDto extends GenericDto implements Serializable {

    private String driver;
    private String connectionString;
    private String userName;
    private String password;
    private Long productId;
    private Long roleDataId;

}
