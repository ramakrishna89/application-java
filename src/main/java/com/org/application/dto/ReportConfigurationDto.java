package com.org.application.dto;

import com.org.application.annotations.IfPresentCheckOtherField;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@IfPresentCheckOtherField.List({
        @IfPresentCheckOtherField(ifPresent = "reportType", checkValue = "1", otherField = "staticQuery"),
        @IfPresentCheckOtherField(ifPresent = "reportType", checkValue = "2", otherField = "tableName"),
        @IfPresentCheckOtherField(ifPresent = "reportType", checkValue = "2", otherField = "columnData"),
        @IfPresentCheckOtherField(ifPresent = "reportType", checkValue = "2", otherField = "groupByData"),
        @IfPresentCheckOtherField(ifPresent = "reportType", checkValue = "2", otherField = "orderByByData"),
        @IfPresentCheckOtherField(ifPresent = "reportType", checkValue = "3", otherField = "customIdentifier"),
})
public class ReportConfigurationDto extends GenericDto implements Serializable {


    @NotNull(message = "app.error.msg-2")
    private Integer reportType;
    @NotNull(message = "app.error.msg-2")
    private Integer moduleType;
    private String staticQuery;
    private String customIdentifier;
    private String tableName;
    private String columnData;
    private String groupByData;
    private String orderByByData;
    @Builder.Default
    private List<ReportSearchConfigurationDto> reportSearchConfigurationDtos = new ArrayList<>(0);

}
