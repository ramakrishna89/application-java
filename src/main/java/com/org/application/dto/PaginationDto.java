package com.org.application.dto;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PaginationDto implements Serializable {

    private int pageIndex;
    private int pageSize;
    private String sortField;
    private String sortOrder;
    private Long total;
    private boolean loading;
    private String searchField;
    private String searchValue;
    private List<Object> result;

}
