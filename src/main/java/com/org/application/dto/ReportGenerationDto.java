package com.org.application.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Data
@SuperBuilder
@AllArgsConstructor
@ToString
public class ReportGenerationDto extends GenericDto implements Serializable {
}
