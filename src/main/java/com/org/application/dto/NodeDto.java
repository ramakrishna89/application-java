package com.org.application.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NodeDto implements Serializable {

    private String title;
    private Long key;
    private Long parentKey;
    private boolean expanded;
    private boolean isLeaf;
    private boolean checked;
    private boolean selectable;

    @Builder.Default
    private List<NodeDto> children = new ArrayList<NodeDto>(0);

}
