package com.org.application.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotificationEmailDto implements Serializable {
    private int type;
    private String recipient;
    private String activationUrl;
    private String tempPassword;
}
