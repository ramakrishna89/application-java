package com.org.application.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
public abstract class GenericDto implements Serializable {

    private Long id;
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String name;
    private String description;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
    private String createdBy;
    private String modifiedBy;
    @NotNull(message = "app.error.msg-2")
    private boolean status;
}
