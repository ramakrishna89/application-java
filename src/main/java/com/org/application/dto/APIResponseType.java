package com.org.application.dto;

public enum APIResponseType {
    MESSAGE(0),
    OBJECT(1),
    LIST(2),
    ;

    private int value;

    APIResponseType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
