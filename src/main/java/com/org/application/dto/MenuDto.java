package com.org.application.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuDto extends GenericDto implements Comparable<MenuDto>, Serializable {

    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String path;
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String iconType;
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String iconTheme;
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String icon;
    @NotNull(message = "app.error.msg-2")
    private Long rootMenuId;
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String menuType;

    @JsonProperty("submenu")
    private List<MenuDto> subMenuDtos = new ArrayList<MenuDto>(0);
    private String key;

    @Override
    public int compareTo(MenuDto o) {
        return this.getId() < o.getId() ? -1 : 1;
    }
}
