package com.org.application.dto;

/*
 * @author "Ramakrishna R" on 17/11/21 : 12:23 pm
 * @Project application-java
 */


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportSearchConfigurationDto extends GenericDto implements Serializable {

    private Integer operationType;
    private Integer fieldType;
    private String dropDownQuery;

}
