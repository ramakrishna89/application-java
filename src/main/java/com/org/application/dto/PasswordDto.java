package com.org.application.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PasswordDto extends GenericDto implements Serializable {

    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String password;

    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String oldPassword;

    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String email;

}
