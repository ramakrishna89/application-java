package com.org.application.exception;

public class APPException extends RuntimeException {

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public APPException(String exMessage) {
        super(exMessage);
    }
}
