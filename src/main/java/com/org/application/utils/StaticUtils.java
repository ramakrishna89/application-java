package com.org.application.utils;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.APIResponseDto;
import com.org.application.dto.APIResponseType;
import com.org.application.dto.PaginationDto;
import com.org.application.exception.APPException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.util.StringUtils;

import java.util.UUID;
import java.util.function.Function;

@Slf4j
public class StaticUtils {

    public static String generateUUID() {
        return UUID.randomUUID().toString();
    }

    public static ResponseEntity<Object> getSuccessResponse(Object o,
                                                            APIResponseType type,
                                                            HttpStatus httpStatus) {
        try {
            return new ResponseEntity<>(new APIResponseDto(
                    true,
                    ApplicationConst.app_success_msg,
                    type.getValue(), o), httpStatus);
        } catch (Exception ex) {
            log.info("Error getSuccessResponse: " + ex);
            return new ResponseEntity<>(new APIResponseDto(false,
                    ApplicationConst.app_failure_api_response_construction_msg,
                    APIResponseType.OBJECT.getValue(), ex), HttpStatus.BAD_REQUEST);
        } finally {
        }
    }

    public static ResponseEntity<Object> getFailureResponse(Object o,
                                                            APIResponseType type) {
        try {
            if (o instanceof APPException) {
                return new ResponseEntity<>(new APIResponseDto(
                        false,
                        ((APPException) o).getMessage(),
                        APIResponseType.MESSAGE.getValue(), o), HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(new APIResponseDto(
                    false,
                    (o != null && StringUtils.hasText(o.toString())) ? o.toString() :
                            ApplicationConst.app_failure_msg,
                    type.getValue(), o), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            log.info("Error getSuccessResponse: " + ex);
            return new ResponseEntity<>(new APIResponseDto(false,
                    ApplicationConst.app_failure_api_response_construction_msg,
                    APIResponseType.OBJECT.getValue(), ex), HttpStatus.BAD_REQUEST);
        } finally {
        }
    }

    public static Function<PaginationDto, PageRequest> generatePageRequest = (dto -> {
        if (StringUtils.hasText(dto.getSortField())) {
            return PageRequest.of((dto.getPageIndex() - 1), dto.getPageSize(), Sort.by(
                    StringUtils.hasText(dto.getSortOrder()) && ApplicationConst.app_sort_order_desc.equals(dto.getSortOrder()) ?
                            Sort.Direction.DESC : Sort.Direction.ASC,
                    dto.getSortField()));
        } else {
            return PageRequest.of((dto.getPageIndex() - 1), dto.getPageSize());
        }
    });

    public static User getCurrentSecurityUser() throws Exception {
        return ((User) SecurityContextHolder.getContext().
                getAuthentication().getPrincipal());
    }

    public static Long getMinutesInMilliSeconds(Integer minutes) {
        return minutes == null ? 0 : Long.valueOf((minutes * 60 * 1000));
    }

}
