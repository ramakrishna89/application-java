package com.org.application.config;

import com.org.application.constants.ApplicationConst;
import com.org.application.props.ApplicationProps;
import com.org.application.service.security.UserDetailsImpl;
import com.zaxxer.hikari.HikariDataSource;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider", dateTimeProviderRef = "dateTimeProvider")
@Slf4j
public class JPAConfig {

    @Bean
    @Primary
    public DataSourceProperties appDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    public HikariDataSource appDataSource() {
        return appDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }

    @Bean
    public AuditorAware<String> auditorProvider() {
        return () -> {
            if (SecurityContextHolder.getContext().getAuthentication() != null) {
                try {
                    UserDetailsImpl principal = (UserDetailsImpl) SecurityContextHolder.getContext()
                            .getAuthentication().getPrincipal();
                    return Optional.of(principal.getActualName());
                } catch (Exception ex) {
                    log.info("No User in Session. AuditorAware exception info : " + ex);
                    return Optional.of(ApplicationConst.app_user_unknown);
                }
            } else {
                return Optional.of(ApplicationConst.app_user_unknown);
            }
        };
    }

    @Bean
    public DateTimeProvider dateTimeProvider() {
        return () -> Optional.of(LocalDateTime.now(ZoneId.systemDefault()));
    }
}
