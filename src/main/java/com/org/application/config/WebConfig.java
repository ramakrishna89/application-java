package com.org.application.config;

import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.org.application.props.ApplicationProps;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.time.format.DateTimeFormatter;

@Configuration
@AllArgsConstructor
public class WebConfig implements WebMvcConfigurer {

    private final ApplicationProps props;


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }


    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {
        return builder -> {
            builder.simpleDateFormat(props.getDatePattern());
            builder.serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern(props.getDatePattern())));
            builder.serializers(new LocalTimeSerializer(DateTimeFormatter.ofPattern(props.getTimePattern())));
            builder.serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(props.getDateTimePattern())));
            builder.deserializers(new LocalDateDeserializer(DateTimeFormatter.ofPattern(props.getDatePattern())));
            builder.deserializers(new LocalTimeDeserializer(DateTimeFormatter.ofPattern(props.getTimePattern())));
            builder.deserializers(new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(props.getDateTimePattern())));
        };
    }


}
