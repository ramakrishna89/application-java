package com.org.application.config;

import com.org.application.props.ApplicationProps;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ApplicationProps.class)
public class PropsConfig {
}
