package com.org.application.controller;


import com.org.application.constants.ApplicationConst;
import com.org.application.dto.APIResponseType;
import com.org.application.dto.AuthDto;
import com.org.application.service.interfaces.AuthenticationService;
import com.org.application.utils.StaticUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@RestController
@RequestMapping(ApplicationConst.app_nav_authentication_page)
@AllArgsConstructor
@Slf4j
public class AuthenticationController {

    private final AuthenticationService service;

    @PostMapping("/check-admin")
    public ResponseEntity<Object> checkAdmin() {
        try {
            if (service.checkAdmin() == 0) {
                return StaticUtils.getSuccessResponse(null,
                        APIResponseType.MESSAGE,
                        HttpStatus.OK);
            }
            return StaticUtils.getFailureResponse(null, APIResponseType.MESSAGE);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody @Valid AuthDto dto, BindingResult result) {
        try {
            if (result.hasErrors()) {
                log.info("Validation failure");
                return StaticUtils.getFailureResponse(result.getAllErrors(), APIResponseType.LIST);
            }
            return StaticUtils.getSuccessResponse(service.authenticateUser(dto),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @PostMapping("/refresh-token")
    public ResponseEntity<Object> refreshToken(@RequestBody @Valid AuthDto dto,
                                               BindingResult result) {
        try {
            if (result.hasErrors()) {
                log.info("Validation failure");
                return StaticUtils.getFailureResponse(result.getAllErrors(),
                        APIResponseType.LIST);
            }
            if (!StringUtils.hasText(dto.getRefreshToken())) {
                return StaticUtils.getFailureResponse("Refresh token should not be empty.",
                        APIResponseType.MESSAGE);
            }
            return StaticUtils.getSuccessResponse(service.refreshToken(dto),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }


    @PostMapping("/logout")
    public ResponseEntity<Object> logout(@RequestBody @Valid AuthDto dto,
                                         BindingResult result) {
        try {
            if (result.hasErrors()) {
                log.info("Validation failure");
                return StaticUtils.getFailureResponse(result.getAllErrors(),
                        APIResponseType.LIST);
            }
            if (!StringUtils.hasText(dto.getRefreshToken())) {
                return StaticUtils.getFailureResponse("Refresh token should not be empty.",
                        APIResponseType.MESSAGE);
            }
            service.deleteAuthRefreshToken(dto.getRefreshToken());
            return StaticUtils.getSuccessResponse("User logged out successfully.",
                    APIResponseType.MESSAGE,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex.getMessage(), APIResponseType.OBJECT);
        }
    }
}
