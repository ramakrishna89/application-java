package com.org.application.controller;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.APIResponseType;
import com.org.application.utils.StaticUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApplicationConst.app_nav_admin_dashboard_page)
@AllArgsConstructor
@Slf4j
@CrossOrigin("*")
public class DashboardController {

    @GetMapping("/admin-dashboard")
    public ResponseEntity<Object> initDashboard() {
        return StaticUtils.getSuccessResponse(null,
                APIResponseType.MESSAGE,
                HttpStatus.OK);
    }
}
