package com.org.application.controller;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.APIResponseType;
import com.org.application.dto.PaginationDto;
import com.org.application.dto.PasswordDto;
import com.org.application.service.interfaces.PasswordService;
import com.org.application.utils.StaticUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(ApplicationConst.app_nav_password_page)
@AllArgsConstructor
@Slf4j
public class PasswordController implements GenericController<PasswordDto> {


    private PasswordService service;

    @Override
    @PostMapping(ApplicationConst.app_nav_create)
    public ResponseEntity<Object> create(@RequestBody @Valid PasswordDto dto, BindingResult result) {
        try {
            if (result.hasErrors()) {
                log.info("Validation failure");
                return StaticUtils.getFailureResponse(result.getAllErrors(), APIResponseType.LIST);
            }
            return StaticUtils.getSuccessResponse(service.create(dto),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @PutMapping(ApplicationConst.app_nav_update)
    public ResponseEntity<Object> update(@RequestBody @Valid PasswordDto dto, BindingResult result) {
        try {
            if (result.hasErrors()) {
                log.info("Validation failure");
                return StaticUtils.getFailureResponse(result.getAllErrors(), APIResponseType.LIST);
            }
            return StaticUtils.getSuccessResponse(service.update(dto),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    public ResponseEntity<Object> readAll(@RequestBody @Valid PaginationDto dto) {
        return null;
    }

    @Override
    public ResponseEntity<Object> read(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<Object> delete(Long id) {
        return null;
    }

    @Override
    @GetMapping(ApplicationConst.app_nav_IdNameMap)
    public ResponseEntity<Object> getIdNameMapping(Long id) {
        try {
            return StaticUtils.getSuccessResponse(service.getIdAndNameMap(),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    public ResponseEntity<Object> changeStatus(Long id, boolean status) {
        return null;
    }
}
