package com.org.application.controller;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.APIResponseType;
import com.org.application.dto.PaginationDto;
import com.org.application.dto.RoleDataDto;
import com.org.application.service.interfaces.RoleDataService;
import com.org.application.utils.StaticUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(ApplicationConst.app_nav_roleData_page)
@AllArgsConstructor
@Slf4j
public class RoleDataController implements GenericController<RoleDataDto> {

    private RoleDataService service;

    @Override
    @PostMapping(ApplicationConst.app_nav_create)
    public ResponseEntity<Object> create(@RequestBody @Valid RoleDataDto dto, BindingResult result) {
        try {
            if (result.hasErrors()) {
                log.info("Validation failure");
                return StaticUtils.getFailureResponse(result.getAllErrors(), APIResponseType.LIST);
            }
            return StaticUtils.getSuccessResponse(service.create(dto),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @PutMapping(ApplicationConst.app_nav_update)
    public ResponseEntity<Object> update(@RequestBody @Valid RoleDataDto dto, BindingResult result) {
        try {
            if (result.hasErrors()) {
                log.info("Validation failure");
                return StaticUtils.getFailureResponse(result.getAllErrors(), APIResponseType.LIST);
            }
            return StaticUtils.getSuccessResponse(service.update(dto),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @PostMapping(ApplicationConst.app_nav_read_all)
    public ResponseEntity<Object> readAll(@RequestBody @Valid PaginationDto dto) {
        try {
            return StaticUtils.getSuccessResponse(service.readAll(dto),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @GetMapping(ApplicationConst.app_nav_read)
    public ResponseEntity<Object> read(Long id) {
        try {
            return StaticUtils.getSuccessResponse(service.read(id),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @DeleteMapping(ApplicationConst.app_nav_delete)
    public ResponseEntity<Object> delete(Long id) {
        try {
            return StaticUtils.getSuccessResponse(service.delete(id),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @GetMapping(ApplicationConst.app_nav_IdNameMap)
    public ResponseEntity<Object> getIdNameMapping(Long id) {
        try {
            return StaticUtils.getSuccessResponse(service.getIdAndNameMap(),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @GetMapping(ApplicationConst.app_nav_ChangeStatus)
    public ResponseEntity<Object> changeStatus(@RequestParam Long id, @RequestParam boolean status) {
        try {
            return StaticUtils.getSuccessResponse(service.changeStatus(id, status),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }
}
