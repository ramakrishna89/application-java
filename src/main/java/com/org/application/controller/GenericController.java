package com.org.application.controller;

import com.org.application.dto.PaginationDto;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

public interface GenericController<T> {

    ResponseEntity<Object> create(T dto, BindingResult result);

    ResponseEntity<Object> update(T dto, BindingResult result);

    ResponseEntity<Object> readAll(PaginationDto dto);

    ResponseEntity<Object> read(Long id);

    ResponseEntity<Object> delete(Long id);

    ResponseEntity<Object> getIdNameMapping(Long id);

    ResponseEntity<Object> changeStatus(Long id, boolean status);

}
