package com.org.application.controller;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.APIResponseType;
import com.org.application.dto.AuthDto;
import com.org.application.dto.UserDto;
import com.org.application.exception.APPException;
import com.org.application.props.ApplicationProps;
import com.org.application.service.interfaces.RegistrationService;
import com.org.application.utils.StaticUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping(ApplicationConst.app_nav_registration_page)
@AllArgsConstructor
@Slf4j
public class RegistrationController {

    private final RegistrationService service;
    private final ApplicationProps props;

    @PostMapping("/admin-sign-up")
    public ResponseEntity<Object> adminSignUp(@RequestBody @Valid UserDto userDto, BindingResult result) {
        try {
            if (result.hasErrors()) {
                log.info("Validation failure");
                return StaticUtils.getFailureResponse(result.getAllErrors(), APIResponseType.LIST);
            }
            return StaticUtils.getSuccessResponse(service.sysAdminSignUp(userDto),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        } finally {
        }
    }

    @PostMapping("/forgot-password")
    public ResponseEntity<Object> forgotPassword(@RequestBody @Valid AuthDto dto) {
        try {
            service.generateVerificationToken(dto.getEmail());
            return StaticUtils.getSuccessResponse(null,
                    APIResponseType.MESSAGE,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @GetMapping("/account-verification/{token}")
    public void accountVerification(@PathVariable String token, HttpServletResponse response) {
        try {
            service.accountActivation(token);
            response.sendRedirect(props.getFrontEndDomain() + props.getActivationMsgURL()
                    + ";msg=app.error-page.account-activation-successful");
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            try {
                if (ex instanceof APPException) {
                    response.sendRedirect(props.getFrontEndDomain() + props.getActivationMsgURL()
                            + ";msg=" + ((APPException) ex).getMessage());
                } else {
                    response.sendRedirect(props.getFrontEndDomain() + props.getActivationMsgURL());
                }
            } catch (Exception ex1) {
                log.info("Error: " + ex1);
            }
        } finally {
        }
    }
}
