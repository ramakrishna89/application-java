package com.org.application.controller;

import com.org.application.constants.ApplicationConst;
import com.org.application.dto.APIResponseType;
import com.org.application.dto.MenuDto;
import com.org.application.dto.PaginationDto;
import com.org.application.dto.RoleAccessDto;
import com.org.application.service.interfaces.MenuService;
import com.org.application.utils.StaticUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(ApplicationConst.app_nav_menu_page)
@AllArgsConstructor
@Slf4j
public class MenuController implements GenericController<MenuDto> {

    private MenuService service;

    @PostMapping("/load-menu-items")
    public ResponseEntity<Object> loadMenuItemsForRole(@RequestBody @Valid List<RoleAccessDto> dtos) {
        try {
            Set<Long> roleIds = new HashSet<>(0);
            for (RoleAccessDto dto : dtos) {
                if (dto.getId() == null || dto.getId().equals(0)) {
                    return StaticUtils.getFailureResponse("app.error.msg-7",
                            APIResponseType.MESSAGE);
                } else {
                    roleIds.add(dto.getId());
                }
            }
            return StaticUtils.getSuccessResponse(service.loadMenuForRole(roleIds),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        } finally {
        }
    }

    @GetMapping("/load-all-menu-types")
    public ResponseEntity<Object> loadAllMenuTypes() {
        try {
            return StaticUtils.getSuccessResponse(service.loadAllMenuTypes(),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @GetMapping("/load-menus-of-menu-type")
    public ResponseEntity<Object> loadMenusOfMenuType(@RequestParam String menuType) {
        try {
            return StaticUtils.getSuccessResponse(service.loadMenusOfMenuType(menuType),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @PostMapping(ApplicationConst.app_nav_create)
    public ResponseEntity<Object> create(@RequestBody @Valid MenuDto dto, BindingResult result) {
        try {
            if (result.hasErrors()) {
                log.info("Validation failure");
                return StaticUtils.getFailureResponse(result.getAllErrors(), APIResponseType.LIST);
            }
            return StaticUtils.getSuccessResponse(service.create(dto),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @PutMapping(ApplicationConst.app_nav_update)
    public ResponseEntity<Object> update(@RequestBody @Valid MenuDto dto, BindingResult result) {
        try {
            if (result.hasErrors()) {
                log.info("Validation failure");
                return StaticUtils.getFailureResponse(result.getAllErrors(), APIResponseType.LIST);
            }
            return StaticUtils.getSuccessResponse(service.update(dto),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @PostMapping(ApplicationConst.app_nav_read_all)
    public ResponseEntity<Object> readAll(@RequestBody @Valid PaginationDto dto) {
        try {
            return StaticUtils.getSuccessResponse(service.readAll(dto),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @GetMapping(ApplicationConst.app_nav_read)
    public ResponseEntity<Object> read(@RequestParam Long id) {
        try {
            return StaticUtils.getSuccessResponse(service.read(id),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @DeleteMapping(ApplicationConst.app_nav_delete)
    public ResponseEntity<Object> delete(@RequestParam Long id) {
        try {
            return StaticUtils.getSuccessResponse(service.delete(id),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @GetMapping(ApplicationConst.app_nav_IdNameMap)
    public ResponseEntity<Object> getIdNameMapping(Long id) {
        try {
            return StaticUtils.getSuccessResponse(service.getIdAndNameMap(),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }

    @Override
    @GetMapping(ApplicationConst.app_nav_ChangeStatus)
    public ResponseEntity<Object> changeStatus(@RequestParam Long id, @RequestParam boolean status) {
        try {
            return StaticUtils.getSuccessResponse(service.changeStatus(id, status),
                    APIResponseType.OBJECT,
                    HttpStatus.OK);
        } catch (Exception ex) {
            log.info("Error: " + ExceptionUtils.getStackTrace(ex));
            return StaticUtils.getFailureResponse(ex, APIResponseType.OBJECT);
        }
    }
}
