package com.org.application.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "app.props")
public class ApplicationProps {

    private String name;
    private String subjectActivation;
    private String regards;
    private String sender;
    private String activateBtn;
    private String company;
    private String companyAddress;
    private String activationMessage;
    private String activationSubMessage;
    private String domainURL;
    private String mailFrom;
    private String activationLink;
    private String keystoreName;
    private String keystorePassword;
    private String keyAlias;
    private Integer jwtTimeoutInMinutes;
    private String timeZone;
    private String frontEndDomain;
    private String activationMsgURL;
    private Integer permissionMode;
    private String datePattern;
    private String dateTimePattern;
    private String timePattern;
    private Integer activationMailDurationInHour;
}
