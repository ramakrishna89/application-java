package com.org.application.constants;

public class ApplicationConst {

    public static final boolean active = true;
    public static final boolean inActive = false;


    public static final String app_role_sysadmin = "SYSADMIN";
    public static final String app_role_sysadmin_code = "ALL";
    public static final String app_user_unknown = "UNKNOWN";
    public static final String app_base_product = "SYSTEM";
    public static final String app_base_product_code = "ALL";

    public static final int app_permission_maker = 1;
    public static final int app_permission_checker = 2;
    public static final int app_permission_authorizer = 3;

    public static final int app_mail_activation = 1;

    public static final String app_success_msg = "app.success.msg-0";
    public static final String app_failure_msg = "app.error.msg-0";
    public static final String app_failure_api_response_construction_msg = "Initiated Operation " +
            "Failed (Due to APIResponse Construction).";

    public static final String app_jwt_header_name = "Authorization";
    public static final String app_jwt_bearer_text = "Bearer";

    public static final Long app_root_menu_id = 0L;
    public static final String app_sort_order_desc = "descend";

    public static final int app_field_type_text_box = 1;
    public static final int app_field_type_date_time_range = 2;
    public static final int app_field_type_query_drop_down = 3;

    public static final int app_module_type_default = 0;
    public static final int app_module_type_pg = 1;
    public static final int app_module_type_sdms = 2;
    public static final int app_module_type_mpcss = 3;

    public static final int app_report_type_static = 1;
    public static final int app_report_type_dynamic = 2;
    public static final int app_report_type_custom = 3;

    public static final int app_report_operation_type_equal = 1;
    public static final int app_report_operation_type_not_equal = 2;
    public static final int app_report_operation_type_like = 3;
    public static final int app_report_operation_type_in = 4;
    public static final int app_report_operation_type_not_in = 5;

    public static final String app_nav_read_all = "/read-all";
    public static final String app_nav_read = "/read";
    public static final String app_nav_create = "/create";
    public static final String app_nav_update = "/update";
    public static final String app_nav_delete = "/delete";
    public static final String app_nav_IdNameMap = "/id-name-map";
    public static final String app_nav_ChangeStatus = "/change-status";

    public static final String app_nav_authentication_page = "/api/authentication-page";
    public static final String app_nav_admin_dashboard_page = "/api/admin-dashboard-page";
    public static final String app_nav_menu_page = "/api/menu-page";
    public static final String app_nav_password_page = "/api/password-page";
    public static final String app_nav_registration_page = "/api/registration-page";
    public static final String app_nav_product_page = "/api/product-page";
    public static final String app_nav_roleAccess_page = "/api/role-access-page";
    public static final String app_nav_roleData_page = "/api/role-data-page";
    public static final String app_nav_user_page = "/api/user-page";
    public static final String app_nav_reportConfiguration_page = "/api/report-configuration-page";
    public static final String app_nav_reportGeneration_page = "/api/report-generation-page";
}
