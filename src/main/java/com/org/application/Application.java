package com.org.application;

import com.org.application.props.ApplicationProps;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.TimeZone;

@SpringBootApplication
public class Application {

    private final ApplicationProps applicationProps;

    public Application(ApplicationProps applicationProps) {
        this.applicationProps = applicationProps;
    }

    @PostConstruct
    public void init() {
        System.out.println("------------------------------------------------------");
        System.setProperty("user.timezone", applicationProps.getTimeZone());
        TimeZone.setDefault(TimeZone.getTimeZone(applicationProps.getTimeZone()));
        System.out.println("DEFAULT TIME ZONE : " + ZoneId.systemDefault());
        System.out.println("USER TIME ZONE    : " + System.getProperty("user.timezone"));
        System.out.println("LOCAL DATE TIME   : " + LocalDateTime.now());
        System.out.println("UTC DATE TIME     : " + Instant.now());
        System.out.println("------------------------------------------------------");
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
