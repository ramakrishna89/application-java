package com.org.application.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class ReportConfiguration extends GenericModel implements Serializable {

    @Column(name = "REPORT_TYPE", nullable = false)
    private Integer reportType;
    @Column(name = "MODULE_TYPE", nullable = false)
    private Integer moduleType;
    @Column(name = "STATIC_QUERY", columnDefinition = "text")
    private String staticQuery;
    @Column(name = "CUSTOM_IDENTIFIER")
    private String customIdentifier;
    @Column(name = "TABLE_NAME")
    private String tableName;
    @Column(name = "COLUMN_DATA")
    private String columnData;
    @Column(name = "GROUP_BY_DATA")
    private String groupByData;
    @Column(name = "ORDER_BY_DATA")
    private String orderByByData;

    @ManyToOne
    @JoinColumn(name = "role_data_id", referencedColumnName = "id")
    private RoleData roleData;

    @OneToMany(mappedBy = "reportConfiguration", fetch = FetchType.LAZY)
    private List<ReportSearchConfiguration> reportSearchConfiguration = new ArrayList<>(0);
}
