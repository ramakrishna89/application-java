package com.org.application.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class Product extends GenericModel implements Serializable {

    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    @Column(name = "CODE", nullable = false)
    private String code;

    @ManyToMany(mappedBy = "productList", fetch = FetchType.LAZY)
    @Singular("roleAccess")
    private List<RoleAccess> roleAccessList = new ArrayList<RoleAccess>(0);

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
    @Singular("dbDetail")
    private List<DBDetail> dbDetails = new ArrayList<>(0);
}
