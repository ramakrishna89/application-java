package com.org.application.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class RoleData extends GenericModel implements Serializable {

    @Column(name = "CODE", unique = true)
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String code;

    @Column(name = "DATA_ID", unique = true)
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private Long dataId;

    @ManyToMany(mappedBy = "roleDataList", fetch = FetchType.LAZY)
    @Singular("roleAccess")
    private List<RoleAccess> roleAccessList = new ArrayList<RoleAccess>(0);

    @OneToMany(mappedBy = "roleData", fetch = FetchType.LAZY)
    private List<ReportConfiguration> reportConfigurationList = new ArrayList<>(0);

    @OneToMany(mappedBy = "roleData", fetch = FetchType.LAZY)
    private List<DBDetail> dbDetailList = new ArrayList<>(0);
}
