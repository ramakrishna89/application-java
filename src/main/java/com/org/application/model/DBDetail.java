package com.org.application.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class DBDetail extends GenericModel implements Serializable {

    @Column(name = "DATA_ID")
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String dataId;

    @Column(name = "DRIVER", nullable = false)
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String driver;

    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    @Column(name = "CONNECTION_STRING", nullable = false)
    private String connectionString;

    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    @Column(name = "USER_NAME", nullable = false)
    private String userName;

    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    @Column(name = "PASSWORD", nullable = false)
    private String password;


    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "ROLE_DATA_ID", referencedColumnName = "id")
    private RoleData roleData;
}
