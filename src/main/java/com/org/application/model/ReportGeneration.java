package com.org.application.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import java.io.Serializable;

@Data
@AllArgsConstructor
@SuperBuilder
@Entity
public class ReportGeneration extends GenericModel implements Serializable {
}
