package com.org.application.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "MENU")
public class Menu extends GenericModel implements Serializable {

    @Column(name = "PATH", unique = true)
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String path;

    @Column(name = "ICON_TYPE")
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String iconType;

    @Column(name = "ICON_THEME")
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String iconTheme;

    @Column(name = "ICON")
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String icon;

    @Column(name = "ROOT_MENU_ID")
    @NotNull(message = "app.error.msg-2")
    private Long rootMenuId;

    @Column(name = "MENU_TYPE")
    @NotBlank(message = "app.error.msg-1")
    @NotNull(message = "app.error.msg-2")
    private String menuType;

    @ManyToMany(mappedBy = "menus", fetch = FetchType.LAZY)
    @Singular
    private List<RoleAccess> roleAccesses = new ArrayList<RoleAccess>(0);
}
