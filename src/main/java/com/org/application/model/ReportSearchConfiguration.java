package com.org.application.model;

/*
 * @author "Ramakrishna R" on 17/11/21 : 12:23 pm
 * @Project application-java
 */


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class ReportSearchConfiguration extends GenericModel implements Serializable {

    @Column(name = "OPERATION_TYPE", nullable = false)
    private Integer operationType;
    @Column(name = "FIELD_TYPE", nullable = false)
    private Integer fieldType;
    @Column(name = "DROP_DOWN_QUERY", nullable = false)
    private String dropDownQuery;

    @ManyToOne
    @JoinColumn(name = "report_configuration_id", referencedColumnName = "id", nullable = false)
    private ReportConfiguration reportConfiguration;

}
