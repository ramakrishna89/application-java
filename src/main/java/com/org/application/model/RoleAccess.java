package com.org.application.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class RoleAccess extends GenericModel implements Serializable {


    @Column(name = "PERMISSION", nullable = false)
    private Integer permission;

    @OneToMany(mappedBy = "roleAccess", fetch = FetchType.LAZY)
    @Singular
    private List<User> users = new ArrayList<User>(0);

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "role_access_menu_mapping",
            joinColumns = @JoinColumn(name = "role_access_id", referencedColumnName = "id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "menu_id", referencedColumnName = "id", nullable = false))
    @Singular("menu")
    private List<Menu> menus = new ArrayList<Menu>(0);

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "role_access_data_mapping",
            joinColumns = @JoinColumn(name = "role_access_id", referencedColumnName = "id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "role_data_id", referencedColumnName = "id", nullable = false))
    @Singular("roleData")
    private List<RoleData> roleDataList = new ArrayList<RoleData>(0);

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "role_access_product_mapping",
            joinColumns = @JoinColumn(name = "role_access_id", referencedColumnName = "id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id", nullable = false))
    @Singular("product")
    private List<Product> productList = new ArrayList<>(0);


}
