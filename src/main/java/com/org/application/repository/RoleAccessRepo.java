package com.org.application.repository;

import com.org.application.model.RoleAccess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleAccessRepo extends GenericRepo, GenericSpecification<RoleAccess>,
        JpaRepository<RoleAccess, Long>, JpaSpecificationExecutor<RoleAccess> {

    Optional<Long> countByNameAndDescription(String name, String description);
}
