package com.org.application.repository;

import com.org.application.model.Password;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PasswordRepo extends JpaRepository<Password, Long>, JpaSpecificationExecutor<Password> {

    Optional<Password> findByIdAndPassword(Long id, String password);
}
