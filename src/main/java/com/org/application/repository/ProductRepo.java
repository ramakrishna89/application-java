package com.org.application.repository;

import com.org.application.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepo extends GenericRepo, GenericSpecification<Product>,
        JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {
}
