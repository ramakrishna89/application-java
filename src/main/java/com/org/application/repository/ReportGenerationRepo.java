package com.org.application.repository;

import com.org.application.model.ReportGeneration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportGenerationRepo extends GenericRepo, GenericSpecification<ReportGeneration>,
        JpaRepository<ReportGeneration, Long>, JpaSpecificationExecutor<ReportGeneration> {
}
