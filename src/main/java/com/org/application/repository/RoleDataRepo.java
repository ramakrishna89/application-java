package com.org.application.repository;

import com.org.application.model.RoleData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDataRepo extends GenericRepo, GenericSpecification<RoleData>,
        JpaRepository<RoleData, Long>, JpaSpecificationExecutor<RoleData> {
}
