package com.org.application.repository;

import com.org.application.model.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface MenuRepo extends GenericRepo, GenericSpecification<Menu>,
        JpaRepository<Menu, Long>, JpaSpecificationExecutor<Menu> {

    List<Menu> findDistinctByRoleAccesses_IdIsInOrderByIdAsc(Set<Long> roleIds);

    @Query("select distinct menuType FROM Menu")
    List<String> findDistinctMenuType();

    List<Menu> findByMenuType(String menuType);

}
