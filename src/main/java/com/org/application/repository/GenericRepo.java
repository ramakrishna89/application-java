package com.org.application.repository;

/*
 * @author "Ramakrishna R" on 17/10/21 : 10:25 am
 * @Project application-java
 */


import java.util.List;
import java.util.Optional;

public interface GenericRepo {

    <T> Optional<T> findByIdAndStatus(Long id, boolean status, Class<T> modelType);

    <T> List<T> findAllByStatus(boolean status, Class<T> modelType);

}
