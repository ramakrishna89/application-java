package com.org.application.repository;

import com.org.application.model.ReportConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportConfigurationRepo extends GenericRepo, GenericSpecification<ReportConfiguration>,
        JpaRepository<ReportConfiguration, Long>, JpaSpecificationExecutor<ReportConfiguration> {
}
