package com.org.application.annotations.validator;

/*
 * @author "Ramakrishna R" on 21/11/21 : 8:52 am
 * @Project application-java
 */


import com.org.application.annotations.IfPresentCheckOtherField;
import com.org.application.constants.ApplicationConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class IfPresentCheckOtherFieldValidator implements ConstraintValidator<IfPresentCheckOtherField, Object> {


    private String ifPresentFieldName;
    private String checkValue;
    private String otherFieldName;

    @Override
    public void initialize(IfPresentCheckOtherField constraintAnnotation) {
        ifPresentFieldName = constraintAnnotation.ifPresent();
        checkValue = constraintAnnotation.checkValue();
        otherFieldName = constraintAnnotation.otherField();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        try {
            String ifPresentFieldValue = BeanUtils.getProperty(o, ifPresentFieldName);
            if (StringUtils.hasText(checkValue) && StringUtils.hasText(ifPresentFieldValue)) {
                String otherFieldValue = BeanUtils.getProperty(o, otherFieldName);
                if (ifPresentFieldValue.equals(String.valueOf(ApplicationConst.app_report_type_static)) &&
                        checkValue.equals(String.valueOf(ApplicationConst.app_report_type_static)) &&
                        !StringUtils.hasText(otherFieldValue)) {
                    return false;
                } else if (ifPresentFieldValue.equals(String.valueOf(ApplicationConst.app_report_type_dynamic)) &&
                        checkValue.equals(String.valueOf(ApplicationConst.app_report_type_dynamic)) &&
                        !StringUtils.hasText(otherFieldValue)) {
                    return false;
                } else if (ifPresentFieldValue.equals(String.valueOf(ApplicationConst.app_report_type_custom)) &&
                        checkValue.equals(String.valueOf(ApplicationConst.app_report_type_dynamic)) &&
                        !StringUtils.hasText(otherFieldValue)) {
                    return false;
                }
            }
        } catch (Exception ex) {
            log.error("Annotation error 1: " + ExceptionUtils.getStackTrace(ex));
        }
        return true;
    }
}
