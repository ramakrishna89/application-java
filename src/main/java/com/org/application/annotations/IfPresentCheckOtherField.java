package com.org.application.annotations;

/*
 * @author "Ramakrishna R" on 21/11/21 : 8:51 am
 * @Project application-java
 */


import com.org.application.annotations.validator.IfPresentCheckOtherFieldValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IfPresentCheckOtherFieldValidator.class)
@Documented
public @interface IfPresentCheckOtherField {

    String message() default "app.error.msg-37";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String ifPresent();

    String checkValue();

    String otherField();

    @Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List {
        IfPresentCheckOtherField[] value();
    }

}
